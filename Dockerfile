FROM node:12-alpine
WORKDIR /app
RUN echo "Copy files"
COPY . /app
EXPOSE 8080
CMD ["yarn", "start"]