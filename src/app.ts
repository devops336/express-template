"use strict";
import express, { Request, Response } from 'express';
import config from './config';


const app = express();

const listeningCallback = () => {
    /* tslint:disable-next-line:no-console */
    console.log(`App is listening on port ${config.startup.port}`);
}

app.get('/', (req: Request, res: Response) => {
    res.sendStatus(200);
})

app.listen(config.startup.port, listeningCallback)
